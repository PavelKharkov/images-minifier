'use strict';

const resizedWidth = 0;

const fs = require('fs');
const path = require('path');

const imagemin = require('imagemin');
const imageminGifsicle = require('imagemin-gifsicle');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');
const imageminSvgo = require('imagemin-svgo');
const imageminWebp = require('imagemin-webp');
const ImageminGm = require('imagemin-gm');
const imageminGm = (process.platform === 'win32') ?
    new ImageminGm('C:\\Program Files\\GraphicsMagick-1.3.31-Q16') :
    new ImageminGm();

const printTime = date => {
    return `[${String(date.getHours()).padStart(2, '0')}:` +
        `${String(date.getMinutes()).padStart(2, '0')}:` +
        `${String(date.getSeconds()).padStart(2, '0')}]`;
};

const startTime = new Date();
console.log(`${printTime(startTime)} Starting...`);

const convertToWebp = process.argv.includes('--webp');
const listFiles = process.argv.includes('--list');

const encoding = 'utf8';
const inputDir = 'input';
const outputDir = 'output';

if (!fs.existsSync(outputDir)) fs.mkdirSync(outputDir);

let processedImages = 0;
let minifiedImages = 0;
let filesListContents = '';

fs.readdirSync(inputDir).forEach(file => {
    processedImages += 1;
    let canBeResized = false;
    const plugins = [];
    const filePath = path.join(inputDir, file);

    switch (path.extname(file)){
        case '.gif':
            canBeResized = true;
            plugins.push(imageminGifsicle({
                interlaced: false
            }));
            break;
        case '.png':
            canBeResized = true;
            plugins.push(imageminPngquant({
                speed: 1,
                quality: [0.6, 0.9]
            }));
            break;
        case '.jpg':
        case '.jpeg':
            canBeResized = true;
            plugins.push(imageminMozjpeg({
                quality: 75
            }));
            break;
        case '.svg':
            plugins.push(imageminSvgo({
                plugins: [{
                    removeViewBox: false,
                    cleanupIDs: false,
                    removeUselessDefs: false
                }]
            }));
            break;
        case '.webp':
            canBeResized = true;
            plugins.push(imageminWebp({
                quality: 75,
                method: 6,
                metadata: 'all'
            }));
            break;
        default:
            processedImages -= 1;
    }

    if (convertToWebp && canBeResized) {
        plugins.push(imageminWebp({
            quality: 100,
            method: 6,
            metadata: 'all'
        }));
    } else {
        processedImages -= 1;
    }

    if (canBeResized && resizedWidth) {
        plugins.push(imageminGm.resize({
            width: resizedWidth
        }));
    }

    if (plugins.length === 0) return;

    imagemin(
        [filePath],
        {
            destination: outputDir,
            glob: false,
            plugins
        })
        .then(() => {
            minifiedImages += 1;
            filesListContents += `${file}\n`;
            if (minifiedImages < processedImages) return;

            if (listFiles) fs.writeFileSync(path.join(outputDir, 'files.txt'), filesListContents, encoding);

            const endTime = new Date();
            console.log(`${printTime(endTime)} Finished after ${endTime.getTime() - startTime.getTime()} ms`);
        })
        .catch(error => {
            console.error(error);
        });
});