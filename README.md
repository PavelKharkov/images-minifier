# Images-minifier

## Начало работы:
```
yarn install
```

Положите файлы изображений в форматах jpg, jpeg, png, gif, svg, webp в папку input\
После этого запустите:
```
yarn minify [--webp] [--list]
```

С флагом --webp файлы будут конвертироваться в формат webp\
С флагом --list будет выведен текстовый файл со списком обработанных файлов

Сгенерированные файлы появятся в папке output